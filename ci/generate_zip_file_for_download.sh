#!/bin/bash

set -e

name="prime-breakout"

cd "`dirname \"$0\"`"
cd "../app/src/main/assets/"

zip -r $name $name
mv $name.zip $name

echo "/* The zip file was added. We have nothing to hide. */" > $name/css/download-zip.css
