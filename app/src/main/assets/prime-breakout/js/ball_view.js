function BallView(root) {
  this.root = root;
  this.element = createChildOf(root, "ball");
}

BallView.prototype.changedPosition = function(ball) {
  this.element.style.left = ball.x - ball.radius + "%";
  this.element.style.top = ball.y - ball.radius + "%";
  this.element.style.width =  ball.radius * 2 + "%";
  this.element.style.height =  ball.radius * 2 + "%";
  //this.element.style.borderRadius =  ball.radius + "%";
}

BallView.prototype.ballRemoved = function() {
  this.root.removeChild(this.element);
}

BallView.prototype.observe = function(ball) {
  ball.onChangedPosition(this);
  ball.onBallRemoved(this);
  this.changedPosition(ball);
}
