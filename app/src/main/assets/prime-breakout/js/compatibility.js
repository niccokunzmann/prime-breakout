
/* Return a shallow copy of this array. */
Array.prototype.copy = function() {
  return this.slice(0, this.length);
}

/* Try to remove one occurence of an element and return its index or -1 */
Array.prototype.remove = function(item) {
  var found = -1;
  var i;
  for (i = 0; i < this.length; i++) {
    if (this[i] == item) {
      found = i;
      break;
    }
  }
  if (found == -1) {
    return -1;
  }
  for (i = found + 1; i < this.length; i++) {
    this[i - 1] = this[i];
  }
  this.pop();
  return found;
}

/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
Array.prototype.shuffle = function () {
    var j, x, i;
    for (i = this.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = this[i];
        this[i] = this[j];
        this[j] = x;
    }
}

Array.prototype.removeRandomElement = function() {
    return this.removeIndex(Math.floor(Math.random() * this.length));
}

Array.prototype.removeIndex = function(index) {
    // see https://stackoverflow.com/a/5767357/1320237
    var element = this[index];
    this.splice(index, 1);
    return element;
}
