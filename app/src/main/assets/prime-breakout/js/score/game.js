
function GameScore() {
  
}

GameScore.prototype.observeGame = function (game) {
  game.achievements.onAchievementCompleted(this);
  game.achievements.onLevelCompleted(this);
  game.points.onPointsChanged(this);
}

