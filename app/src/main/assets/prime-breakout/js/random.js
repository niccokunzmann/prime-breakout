/* Create a random instance so everyone plays the same games and you can create strategies. */
function Random() {
  this.current = 1;
  this.max = 1000;
  this.step = 763;
}

Random.prototype.nextInt = function(max) {
  var current = this.current;
  for (var i = 0; i < this.current; i++) {
    current = (current * this.step) % this.max;
  }
  this.current = current;
  return this.current % max;
}

Random.prototype.removeElement = function(array) {
  return array.removeIndex(this.nextInt(array.length));
}

Random.prototype.floatBetween = function(min, max) {
  if (min > max) {
    var t = min;
    min = max;
    max = min;
  }
  var int = this.nextInt(this.max);
  return min + (max - min) * int / this.max;
}
