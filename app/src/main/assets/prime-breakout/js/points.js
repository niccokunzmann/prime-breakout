
function Points(settings) {
  this.settings = settings;
  this.pointsToAdd = [];
  for (var i = 0; i < settings.splitUpPointsIntoPieces; i++) {
    this.pointsToAdd.push(0);
  }
  this.points = settings.start;
  this.onPointsChanged = ObserverAttribute("pointsChanged");
}

Points.prototype.itemCreated = function(item) {
  item.onItemCollected(this);
  item.onItemMissed(this);
};

Points.prototype.itemCollected = function(item) {
  item.forPoints().collected(this);
};

Points.prototype.itemMissed = function(item) {
  item.forPoints().missed(this);
};

Points.prototype.achievementCompleted = function(achievement) {
  this.add(achievement.getPoints());
};

Points.prototype.add = function(number) {
  var remainder = number % this.pointsToAdd.length;
  var perStep = (number - remainder) / this.pointsToAdd.length;
  for (var i = this.pointsToAdd.length - 1; i >= 0; i--) {
    this.pointsToAdd[i] += perStep;
    if (remainder != 0) {
      var add = remainder < 0 ? -1 : 1;
      remainder -= add;
      this.pointsToAdd[i] += add;
    }
  }
}

Points.prototype.step = function() {
  this.points += this.pointsToAdd.pop();
  this.pointsToAdd = [0].concat(this.pointsToAdd);
  if (this.points < this.settings.min) {
    this.points = this.settings.min;
  }
  this.onPointsChanged.notify(this);
}

Points.prototype.remove = function(number) {
  this.add(-number);
}

Points.prototype.rewardMiss = function() {
  this.add(this.settings["reward for missed non-prime number"]);
}

Points.prototype.getPoints = function() {
  return this.points;
}

Points.prototype.createViewOn = function(root) {
  var view = new PointsView(root);
  view.observe(this);
}
