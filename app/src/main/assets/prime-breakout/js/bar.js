
function Bar(settings) {
  this.settings = settings;
  this.onChangePosition = new ObserverAttribute("changePosition");
}

Bar.prototype.createViewOn = function (element) {
  var barView = new BarView(element);
  barView.observe(this);
}

Bar.prototype.start = function () {
  this.x = this.settings.startX;
  this.onChangePosition.notify(this);
}

Bar.prototype.goRight = function () {
  //console.log("Bar.goRight");
  this.x += this.settings.speed;
  this.x = this.x + this.getWidth() > this.settings.maxX ? this.settings.maxX - this.getWidth() : this.x;
  this.onChangePosition.notify(this);
}

Bar.prototype.goLeft = function () {
  //console.log("Bar.goLeft");
  this.x -= this.settings.speed;
  this.x = this.x < this.settings.minX ? this.settings.minX : this.x;
  this.onChangePosition.notify(this);
}

Bar.prototype.getX = function () {
  return this.x;
}

Bar.prototype.getY = function () {
  return this.settings.y;
}


Bar.prototype.getWidth = function () {
  return this.settings.width;
}

Bar.prototype.ballTouchesGround = function (ball) {
  if (this.isTouchingIfFallen(ball)) {
    ball.bounceOffPoint({
      x: this.getX() + this.getWidth() / 2,
      y: this.getY() + this.settings.yBounceOffset
    });
  }
}

Bar.prototype.isTouchingIfFallen = function (fallingObject) {
  return fallingObject.getLeft() < this.getRight() && fallingObject.getRight() > this.getLeft();
}

Bar.prototype.getLeft = function (fallingObject) {
  return this.getX();
}

Bar.prototype.getRight = function (fallingObject) {
  return this.getX() + this.getWidth();  
}

Bar.prototype.itemTouchesGround = function (item) {
  if (this.isTouchingIfFallen(item)) {
    item.collect();
  }
}



Bar.prototype.toString = function () {
  return "Bar(" + this.getX() + " to " + this.getX() + this.getWidth() + ")";
}
