
var SMALL_TIMES_TABLES = [];
for (var a = 1; a <= 10; a++) {
  for (var b = 1; b <= 10; b++) {
    if (!SMALL_TIMES_TABLES.includes(a * b)) {
      SMALL_TIMES_TABLES.push(a * b);
    }
  }
}

var BIG_TIMES_TABLES = [];
for (var a = 1; a <= 20; a++) {
  for (var b = 1; b <= 20; b++) {
    if (!BIG_TIMES_TABLES.includes(a * b)) {
      BIG_TIMES_TABLES.push(a * b);
    }
  }
}

function isOne(n) {
  return n == 1;
}

function isMultipleOf3(n) {
  return n % 3 == 0;
}

function isMultipleOf5(n) {
  return n % 5 == 0;
}

function isMultipleOf11(n) {
  return n % 11 == 0;
}

function is42(n) {
  return n == 42;
}

function isPrime(n) {
  if (n <= 1) {
    return false;
  }
  for (var i = 2; i*i <= n; i++) {
    if (n % i == 0) {
      return false;
    }
  }
  return true;
}

function isPrimeTwin(n) {
  return isPrime(n) && (isPrime(n + 2) || isPrime(n - 2));  
}

function isCloseToPrimeTwin(n) {
  return isPrimeTwin(n) || isPrimeTwin(n + 2) || isPrimeTwin(n - 2);  
}

function isEven(n) {
  return n % 2 == 0;
}

function isInSmallTimesTablesAndNotASquare(n) {
  return !isSquare(n) && SMALL_TIMES_TABLES.includes(n);
}

function isInBigTimesTablesAndNotASquare(n) {
  return !isSquare(n) && BIG_TIMES_TABLES.includes(n);
}

function isSquare(n) {
  var root = Math.floor(Math.sqrt(n));
  return root * root == n;
}

function isSquareGreaterOne(n) {
  return n > 1 && isSquare(n);
}

function greatestCommonDivider(a, b) {
  while (a != 0) {
    var c = a
    a = b % a;
    b = c;
  }
  return b;
}

function isChallenge(n) {
  return function(m) {
    return m % n == 0 && m >= n*n && m <= n*n*2;
  }  
}

var isChallenge4 = isChallenge(4);
var isChallenge9 = isChallenge(9);
var isChallenge11 = isChallenge(11);

function isFibonacci(n) {
  var a = 0;
  var b = 0;
  var c = 1;
  while (c < n) {
    a = b;
    b = c;
    c = a + b;
  }
  return c == n;
}

