
function Achievements(settings) {
  this.settings = settings;
  this.onCollect = ObserverAttribute("add");
  this.onMiss = ObserverAttribute("add");
  this.onAchievementCompleted = ObserverAttribute("achievementCompleted");

  this.addDefaultAchievements();
}

Achievements.prototype.addDefaultAchievements = function() {
  // special achievements
  this.points = new PointsAchievement(this.settings.points);
  this.points.onAchievementCompleted(this);

  this.levelCompleted = new LevelAchievement(this.settings.level);
  this.levelCompleted.onAchievementCompleted(this);
  
  this.gameOver = new GameOverAchievement(this.settings.gameOver);
  this.gameOver.onAchievementCompleted(this);
  
  // special numbers
  this.newCollectionAchievements(isOne, this.settings.one, SelectiveSetOfNumbers, this.onCollect);

  this.newCollectionAchievements(is42, this.settings.number42, SelectiveSetOfNumbers, this.onCollect);

  // division rules
  this.newCollectionAchievements(isEven, this.settings.evenNumbers, SelectiveSetOfNumbers, this.onMiss);
  
  this.newCollectionAchievements(isPrime, this.settings.primeNumbers, SelectiveSetOfNumbers, this.onCollect);

  this.newCollectionAchievements(isMultipleOf3, this.settings.multiplesOf3, SelectiveSetOfNumbers, this.onMiss);

  this.newCollectionAchievements(isMultipleOf5, this.settings.multiplesOf5, SelectiveSetOfNumbers, this.onMiss);

  this.newCollectionAchievements(isMultipleOf11, this.settings.multiplesOf11, SelectiveSetOfNumbers, this.onMiss);

  this.newCollectionAchievements(isInSmallTimesTablesAndNotASquare, this.settings.smallTimesTables, SelectiveSetOfNumbers, this.onMiss);

  this.newCollectionAchievements(isInBigTimesTablesAndNotASquare, this.settings.bigTimesTables, SelectiveSetOfNumbers, this.onMiss);

}

Achievements.prototype.enableTwins = function () {
  this.newCollectionAchievements(isCloseToPrimeTwin, this.settings.primeTwins, PrimeTwinSet, this.onCollect);
}

Achievements.prototype.enableSquares = function () {
  var me = this;
  this.newCollectionAchievements(isSquareGreaterOne, this.settings.squares, SelectiveSetOfNumbers, this.onCollect);

  this.settings.challenges.forEach(function(settings) {
    me.newCollectionAchievements(isChallenge(settings.number), settings, SelectiveSetOfNumbers, me.onCollect);
  });
}

Achievements.prototype.observeGame = function (game) {
  game.onLevelCompleted(this.levelCompleted);
  game.onGameOver(this.gameOver);
  game.onStart(this);
  game.onItemCreated(this);
  game.points.onPointsChanged(this.points);
  game.modes.enableModes(this);
}

Achievements.prototype.start = function () { 
  AchievementStorage.forLastGame().reset().observe(this);
  AchievementStorage.forAllGames().observe(this);
}

Achievements.prototype.newCollectionAchievements = function (selection, settings, Collection, notify) {
  var me = this;
  settings.stages.forEach(function(stage) {
    var collection = new Collection(selection);
    var achievement = new CollectionAchievement(stage);
    achievement.observeCollection(collection);
    achievement.onAchievementCompleted(me);
    notify(collection);
  });
}

Achievements.prototype.achievementCompleted = function(achievement) {
  this.onAchievementCompleted.notify(achievement);
}

Achievements.prototype.itemCreated = function (item) {
  item.onItemCollected(this);
  item.onItemMissed(this);
}

Achievements.prototype.itemCollected = function (item) {
  var achieved = item.forAchievements();
  this.onCollect.notify(achieved);
}

Achievements.prototype.itemMissed = function (item) {
  var achieved = item.forAchievements();
  this.onMiss.notify(achieved);
}

Achievements.prototype.createViewOn = function(root) {
  var view = new AchievementsView(root);
  view.observe(this);
}
