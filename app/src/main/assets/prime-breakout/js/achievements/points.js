
function PointsAchievement(settings) {
  this.onAchievementCompleted = ObserverAttribute("achievementCompleted");
  this.points = 0;
  this.settings = settings;
}

PointsAchievement.prototype.pointsChanged = function (points) {
  this.points = points.getPoints();
  this.onAchievementCompleted.notify(this);
}

PointsAchievement.prototype.getPoints = function () {
  return 0;
}

PointsAchievement.prototype.addViewTo = function (root) {
  // we do not need a view, points are displayed
}

PointsAchievement.prototype.storeIn = function (storage) {
  var points = this.points;
  storage.addMaximumPoints(points);
  this.settings.modes.forEach(function(stage) {
    if (points >= stage.points) {
      storage.addMode(stage.mode);
    }
  });
}
