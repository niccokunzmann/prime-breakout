
function PrimeTwinSet(includes) {
  this.numbers = new SelectiveSetOfNumbers(includes);
  this.twins = new SelectiveSetOfNumbers(isPrimeTwin);
  this.onCollectionChanged = ObserverAttribute("collectionChanged");
}

PrimeTwinSet.prototype.add = function(n) {
  this.numbers.add(n);
  this.twins.add(n);
  this.onCollectionChanged.notify(this);
}

PrimeTwinSet.prototype.size = function() {
  var twins = this.twins.asArray();
  twins.sort();
  var others = this.numbers.asArray();
  twins.forEach(function(n){
    others.remove(n);
  });
  var twinCount = 0;
  for (var i = 1; i < twins.length; i++) {
    var n = twins[i];
    if (twins[i - 1] == n - 2) {
      // got a twin!
      if (!others.includes(n + 2) && !others.includes(n - 4)) {
        twinCount++;
      }
    }
  }
  return twinCount;
}
