
function CollectionAchievement(settings) {
  this.settings = settings;
  this.onAchievementCompleted = ObserverAttribute("achievementCompleted");
  this.notified = false;
}

CollectionAchievement.prototype.observeCollection = function (collection) {
  collection.onCollectionChanged(this);
}

CollectionAchievement.prototype.collectionChanged = function (collection) {
  if (collection.size() == this.settings.count && !this.notified) {
    this.onAchievementCompleted.notify(this);
    this.notified = true;
  }
}

CollectionAchievement.prototype.getPoints = function () {
  return this.settings.reward;
}

CollectionAchievement.prototype.addViewTo = function (root) {
  var element = createChildOf(root, "achievement");
  var message = createChildOf(element, this.settings.cls);
  message.innerText = translate(this.settings.message)
  var points = createChildOf(element, "achievement-points");
  points.innerText = (this.getPoints() > 0 ? "+" : "") + (this.getPoints() == 0 ? "" : this.getPoints()); 
}

CollectionAchievement.prototype.storeIn = function (storage) {
  if (this.settings.id != undefined) {
    storage.addId(this.settings.id);
  }
}
