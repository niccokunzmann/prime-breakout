
function SelectiveSetOfNumbers(includes) {
  this.includes = includes;
  this.collected = [];
  this.onCollectionChanged = ObserverAttribute("collectionChanged");
}

SelectiveSetOfNumbers.prototype.size = function() {
  return this.collected.length;
}

SelectiveSetOfNumbers.prototype.add = function(number) {
  if (this.includes(number) && !this.collected.includes(number)) {
    this.collected.push(number);
    this.onCollectionChanged.notify(this);
  }
}

SelectiveSetOfNumbers.prototype.asArray = function() {
  return this.collected.concat([]);
}