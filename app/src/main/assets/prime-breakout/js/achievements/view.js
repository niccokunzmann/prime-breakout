function AchievementsView(root) {
  this.element = createChildOf(root, "achievements");
}

AchievementsView.prototype.achievementCompleted = function(achievement) {
  achievement.addViewTo(this.element);
}

AchievementsView.prototype.observe = function(subject) {
  subject.onAchievementCompleted(this);
}
