
function LevelAchievement(settings) {
  this.currentLevel = settings.start;
  this.settings = settings;
  this.onAchievementCompleted = ObserverAttribute("achievementCompleted");
}

LevelAchievement.prototype.levelCompleted = function (game) {
  this.onAchievementCompleted.notify(this);
  this.currentLevel++;
  game.createNewLevel();
}

LevelAchievement.prototype.getPoints = function () {
  return 0;
}

LevelAchievement.prototype.addViewTo = function (root) {
  var element = createChildOf(root, "achievement");
  var message = translate(this.settings.message);
  element.innerText = message.replace("0", this.currentLevel + "");
}

LevelAchievement.prototype.storeIn = function (storage) {
  storage.addLevelCompleted(this.currentLevel);
}
