
var SIGN_COMPLETED = "☑";
var SIGN_NOT_COMPLETED = "☐";
var ATTR_COUNT = "count";
var ATTR_NEXT = "next";
var ATTR_ACHIEVEMENT_ID = "achievement";

var DISPLAY_FOR_ALL_GAMES = {
  achieved: function (element) {
    element.classList.add("achieved");
    element.classList.remove("next");
    addToHeading(element, SIGN_COMPLETED);
  },
  notAchieved: function (element) {
    addToHeading(element, SIGN_NOT_COMPLETED);
  },
  next: function(element) {
    element.classList.add("next");
  }
};

var DISPLAY_FOR_LAST_GAME = {
  achieved: function (element) {
    element.classList.add("last-game");
  },
  notAchieved: function(element) {},
  next: function(element) {},
};

function enableMode(id) {
  return function() {
    console.log("enable " + id);
    var elements = document.getElementsByClassName(id);
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove(id);
    }
  }
}

var MODES = {
  enableTwins: enableMode("mode-twins"),
  enableSquares: enableMode("mode-squares"),
}


function addToHeading(element, text) {
  var headings = element.getElementsByClassName("heading");
  if (headings.length == 0) {
    return;
  }
  headings[0].innerText = text + " " + headings[0].innerText;
}

function displayAchievement(element, storage, displayStrategy) {
  var id = element.hasAttribute(ATTR_ACHIEVEMENT_ID) ? element.getAttribute(ATTR_ACHIEVEMENT_ID) : element.id;
  var achieved = false;
  if (element.hasAttribute(ATTR_COUNT)) {
    var count = Number(element.getAttribute(ATTR_COUNT));
    achieved = count <= storage.getCountOfId(id);
  } else {
    achieved = storage.includesId(id);
  }
  if (achieved) {
    displayStrategy.achieved(element);
    if (element.hasAttribute(ATTR_NEXT)) {
      var nextId = element.getAttribute(ATTR_NEXT);
      var next = document.getElementById(nextId);
      displayStrategy.next(next);
    }
  } else {
    displayStrategy.notAchieved(element);
  }
}

function displayAchievements() {
  var all = AchievementStorage.forAllGames();
  var last = AchievementStorage.forLastGame();
  var achievements = document.getElementsByClassName("achievement");
  for (var i = 0; i < achievements.length; i++) {
    var element = achievements[i];
    displayAchievement(element, all, DISPLAY_FOR_ALL_GAMES);
    displayAchievement(element, last, DISPLAY_FOR_LAST_GAME);
  }
  getCurrentGameModes().enableModes(MODES);
}

function setDescriptionClick(element) {
  var originalText = element.innerText;
  element.innerText = "⍰";
  function click(event) {
    var t = element.innerText;
    element.innerText = originalText;
    originalText = t;
    element.parentElement.classList.toggle("described");
    event.stopPropagation();
  }
  element.addEventListener("click", click);
  element.parentElement.addEventListener("click", click);
}

function hideDescriptions() {
  var descriptions = document.getElementsByClassName("description");
  for (var i = 0; i < descriptions.length; i++) {
    var element = descriptions[i];
    setDescriptionClick(element);
  }
}

function notifyAboutPersistence() {
  var storage = AchievementStorage.forAllGames();
  if (storage.isPersistent()) {
    document.getElementById("persistence-note").remove();
  }
}

window.addEventListener("load", function() {
  hideDescriptions();
  displayAchievements();
  notifyAboutPersistence();
});
