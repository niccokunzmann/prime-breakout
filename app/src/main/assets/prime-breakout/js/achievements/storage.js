
var ACHIEVEMENT_STORAGE_VERSION = 0;
var ID_LEVELS_REACHED = "level-reached";
var ID_POINTS_REACHED = "points-reached";
var ID_GAMES_PLAYED = "games-played";
var ID_PREFIX_MODE = "mode-";

function AchievementStorage(storage) {
  this.storage = storage;
}

AchievementStorage.forLastGame = function () {
  var storage = new AchievementStorage(LAST_GAME);
  storage.initialize();
  return storage;
}

AchievementStorage.forAllGames = function () {
  var storage = new AchievementStorage(ALL_GAMES);
  storage.initialize();
  return storage;
}

AchievementStorage.prototype.isPersistent = function () {
  return this.storage.isPersistent();
}

AchievementStorage.prototype.observe = function(achievements) {
  achievements.onAchievementCompleted(this);
}

AchievementStorage.prototype.reset = function() {
  this.storage.set(null);
  this.initialize();
  return this;
}

AchievementStorage.prototype.initialize = function() {
  if (this.storage.get() == null || this.storage.get().version != ACHIEVEMENT_STORAGE_VERSION) {
    this.storage.set({
      version: ACHIEVEMENT_STORAGE_VERSION,
      specs: {},
    });
  }
}

AchievementStorage.prototype.achievementCompleted = function(achievement) {
  achievement.storeIn(this);
}

AchievementStorage.prototype.persistent = function(callback) {
  this.initialize();
  var spec = this.storage.get();
  this._callback = callback;
  this._callback(spec.specs);
  this.storage.set(spec);
}

AchievementStorage.prototype.addId = function(id) {
  if (!this.includesId(id)) {
    this.add({
      id: id,
    });
  }
}

AchievementStorage.prototype.add = function(spec) {
  this.persistent(function(specs) {
    specs[spec.id] = spec;
  });
}

AchievementStorage.prototype.addIfHigher = function(spec) {
  this.persistent(function(specs) {
    if (specs[spec.id] == undefined || specs[spec.id].count < spec.count) {
      specs[spec.id] = spec;
    }
  });
}

AchievementStorage.prototype.addLevelCompleted = function(level) {
  this.addIfHigher({
    id: ID_LEVELS_REACHED,
    count: level,
  });
}

AchievementStorage.prototype.addMaximumPoints = function(points) {
  this.addIfHigher({
    id: ID_POINTS_REACHED,
    count: points,
  });
}

AchievementStorage.prototype.getMaximumPoints = function() {
  return this.getCountOfId(ID_POINTS_REACHED);
}

AchievementStorage.prototype.increaseGamesPlayed = function() {
  this.persistent(function(specs) {
    if (specs[ID_GAMES_PLAYED] == undefined) {
      specs[ID_GAMES_PLAYED] = {
        id: ID_GAMES_PLAYED,
        count: 1,
      };
    } else {
      specs[ID_GAMES_PLAYED].count++;
    }
  });
}

AchievementStorage.prototype.includesId = function(id) {
  return this.storage.get().specs[id] != undefined;
}

AchievementStorage.prototype.isInMode = function(mode) {
  return this.includesId(ID_PREFIX_MODE + mode);
}

AchievementStorage.prototype.addMode = function(mode) {
  return this.addId(ID_PREFIX_MODE + mode);
}

AchievementStorage.prototype.getCountOfId = function(id) {
  var spec = this.storage.get().specs[id];
  return spec != undefined ? spec.count : 0;
}

AchievementStorage.prototype.getGamesPlayed = function() {
  return this.getCountOfId(ID_GAMES_PLAYED);
}
