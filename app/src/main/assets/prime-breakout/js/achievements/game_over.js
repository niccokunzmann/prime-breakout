
function GameOverAchievement(settings) {
  this.settings = settings;
  this.onAchievementCompleted = ObserverAttribute("achievementCompleted");
}

GameOverAchievement.prototype.getPoints = function () {
  return 0;
}

GameOverAchievement.prototype.addViewTo = function (root) {
  var element = createChildOf(root, "achievement");
  var key = this.gameNumber == 1 ? this.settings.messageFirstGame : this.settings.messageGamesCompleted;
  var message = translate(key);
  element.innerText = message.replaceAll("0", this.gameNumber + "");
}

GameOverAchievement.prototype.gameOver = function (game) {
  this.onAchievementCompleted.notify(this);
}

GameOverAchievement.prototype.storeIn = function (storage) {
  storage.increaseGamesPlayed();
}
