
function LeftRightBlockMovement(settings) {
  this.settings = settings;
  this.velocity = settings.velocity;
}

LeftRightBlockMovement.prototype.registerOnStepper = function(stepper) {
  this.stepper = stepper;
  stepper.onStep(this);
}

LeftRightBlockMovement.prototype.movesLeft = function () {
  return this.velocity < 0;
}

LeftRightBlockMovement.prototype.step = function() {
  //console.log("movement", this);
  this.block.moveRight(this.velocity);
  var x, wall;
  if (this.movesLeft()) {
    x = this.block.getLeft();
    wall = this.block.getWallToTheLeft();
    this.velocity = x <= wall ? -this.velocity : this.velocity;
  } else {
    x = this.block.getRight();
    wall = this.block.getWallToTheRight();
    this.velocity = x >= wall ? -this.velocity : this.velocity;
  }
}

LeftRightBlockMovement.prototype.forBlock = function(block) {
  block.onBlockDestroyed(this);
  this.block = block;
  return this;
}

LeftRightBlockMovement.prototype.blockDestroyed = function(block) {
  this.stepper.onStep.unregister(this);
}
