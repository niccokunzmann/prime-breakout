
function Block(settings) {
  this.settings = settings;
  this.onBlockDestroyed = ObserverAttribute("blockDestroyed");
  this.onChangedPosition = ObserverAttribute("changedPosition");
  this.onItemCreated = ObserverAttribute("itemCreated");
  this.tags = new TagSet();
  this.itemStrategy = settings.itemStrategy;
  this.movementStrategy = this.settings.movementStrategy.forBlock(this);
  this.x = this.settings.x;
}

Block.prototype.getNumber = function(ball) {
  return this.settings.itemStrategy.getNumber();
}

Block.prototype.getLeft = function() {
  return this.x;
}

Block.prototype.getWidth = function() {
  return this.settings.width;
}

Block.prototype.getRight = function() {
  return this.getLeft() + this.getWidth();
}

Block.prototype.getTop = function() {
  return this.settings.y;
}

Block.prototype.getHeight = function() {
  return this.settings.height;
}

Block.prototype.getBottom = function() {
  return this.getTop() + this.getHeight();
}

Block.prototype.getBottomLeft = function() {
  return {
    x: this.getLeft(),
    y: this.getBottom()
  };
}

Block.prototype.getBottomRight = function() {
  return {
    x: this.getRight(),
    y: this.getBottom()
  };
}

Block.prototype.getTopLeft = function() {
  return {
    x: this.getLeft(),
    y: this.getTop()
  };
}

Block.prototype.getTopRight = function() {
  return {
    x: this.getRight(),
    y: this.getTop()
  };
}

Block.prototype.getCenter = function() {
  return {
    x: (this.getRight() + this.getLeft()) / 2,
    y: (this.getTop() + this.getBottom()) / 2
  };
}

Block.prototype.bounceOffBall = function(ball) {
  // first, get general distance
  if (ball.getRight() < this.getLeft() || 
      ball.getLeft() > this.getRight() ||
      ball.getTop() > this.getBottom() ||
      ball.getBottom() < this.getTop()) {
    return;
  }
  // bounce off the sides
  // remember, we did the overall check if the ball collides
  var collided = false;
  if (ball.isCollidingWithHorizontalLine(this.getTop())) {
    ball.bounceUpOff(this.getTop());
    collided = true;
  }
  if (ball.isCollidingWithHorizontalLine(this.getBottom())) {
    ball.bounceDownOff(this.getBottom());
    collided = true;
  }
  if (ball.isCollidingWithVerticalLine(this.getRight())) {
    ball.bounceRightOff(this.getRight());
    collided = true;
  }
  if (ball.isCollidingWithVerticalLine(this.getLeft())) {
    ball.bounceLeftOff(this.getLeft());
    collided = true;
  }
  if (collided) {
    this.collidedWithBall(ball);
  }
}

Block.prototype.collidedWithBall = function(ball) {
//  console.log("Collision:", this, ball);
  this.createItem();
  this.destroy();
}

Block.prototype.destroy = function() {
  this.settings.blocksInRow.remove(this);
  this.settings.blocksInColumn.remove(this);
  this.onBlockDestroyed.notify(this);
}

Block.prototype.createViewOn = function (element) {
  var view = new BlockView(element);
  view.observe(this);
}

Block.prototype.createItem = function () {
  var item = new FallingItem(this.itemStrategy, this.getCenter(), this.settings.items);
//  console.log("BLOCK: new item", this, item);
  this.onItemCreated.notify(item);
}

Block.prototype.registerOnStepper = function(stepper) {
  this.movementStrategy.registerOnStepper(stepper);
}

Block.prototype.getWallToTheLeft = function() {
  var max = this.getRight();
  return this.settings.blocksInRow.filter(function(block) {
    return block.getRight() < max;
  }).reduce(function(a, block) {
    return Math.max(a, block.getRight()); 
  }, this.settings.movement.minX);
}

Block.prototype.getWallToTheRight = function() {
  var min = this.getLeft();
  return this.settings.blocksInRow.filter(function(block) {
    return block.getLeft() > min;
  }).reduce(function(a, block) {
    return Math.min(a, block.getLeft()); 
  }, this.settings.movement.maxX);
}

Block.prototype.moveRight = function(xVelocity) {
  this.x = Math.max(this.settings.movement.minX, Math.min(this.settings.movement.maxX, this.x + xVelocity));
  this.onChangedPosition.notify(this);
}
