function BlockView(root) {
  this.root = root;
  this.element = createChildOf(root, "block");
}

BlockView.prototype.changedPosition = function(block) {
  this.element.style.left = block.getLeft() + "%";
  this.element.style.top = block.getTop() + "%";
  this.element.style.width =  block.getWidth() + "%";
  this.element.style.height =  block.getHeight() + "%";
  this.element.innerText = block.getNumber() + "";
  this.element.id = "block-" + block.getNumber() + "";
}

BlockView.prototype.blockDestroyed = function() {
  this.root.removeChild(this.element);
}

BlockView.prototype.itemCreated = function(item) {
  item.createViewOn(this.root);
}

BlockView.prototype.tagChanged = function(tags) {
  tags.adjustClassList(this.element.classList);
}

BlockView.prototype.observe = function(block) {
  block.onChangedPosition(this);
  block.onBlockDestroyed(this);
  block.tags.onTagChanged(this);
  block.onItemCreated(this);
  this.changedPosition(block);
  this.tagChanged(block.tags);
}
