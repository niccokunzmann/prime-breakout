function BlockFactory(settings) {
  this.settings = settings;
  this.nextBlockNumber = settings.initialBlockNumber;
  this.numberRandom = new Random();
  this.speedRandom = new Random();
}

BlockFactory.prototype.getBlockX = function (column) {
   return this.settings.minX + (this.settings.maxX - this.settings.minX) * column / this.settings.columns + this.settings.spaceBetweenBlocksX / 2;
}

BlockFactory.prototype.getBlockY = function (row) {
   return this.settings.minY + (this.settings.maxY - this.settings.minY) * row / this.settings.rows + this.settings.spaceBetweenBlocksY / 2;
}

BlockFactory.prototype.addBlocksTo = function (blockContainer) {
  var blocksInRow = {};
  var blocksInColumn = {};
  var positions = [];
  var row;
  var column;
  for (row = 0; row < this.settings.rows; row++) {
    for (column = 0; column < this.settings.columns; column++) {
      positions.push({
        row: row,
        column: column,
      });
      blocksInRow[row] = [];
      blocksInColumn[column] = [];
    }
  }
  while (positions.length > 0) {
    var position = this.numberRandom.removeElement(positions);
    row = position.row;
    column = position.column;
    var block = new Block(Object.assign({
      x: this.getBlockX(column),
      y: this.getBlockY(row),
      blocksInRow: blocksInRow[row],
      blocksInColumn: blocksInColumn[column],
      width: this.getBlockX(column + 1) - this.getBlockX(column) - this.settings.spaceBetweenBlocksX,
      height: this.getBlockY(row + 1) - this.getBlockY(row) - this.settings.spaceBetweenBlocksY,
      itemStrategy: getItemStrategyForNumber(this.nextBlockNumber),
      movementStrategy: this.getMovementStrategyForNumber(this.nextBlockNumber),
      items: this.settings.items,
      states: this.settings.states,
    }, this.settings));
    blockContainer.addBlock(block);
    blocksInRow[row].push(block);
    blocksInColumn[column].push(block);
    this.nextBlockNumber++;
  }
}

BlockFactory.prototype.getMovementStrategyForNumber = function(number) {
  if (isFibonacci(number)) {
    var settings = Object.assign({
      velocity: this.speedRandom.floatBetween(-this.settings.movement.maxVelocity, this.settings.movement.maxVelocity)
    }, this.settings.movement);
    return new LeftRightBlockMovement(settings);
  }
  return new NoBlockMovement();
}
