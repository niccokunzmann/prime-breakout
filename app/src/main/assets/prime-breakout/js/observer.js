
function ObserverAttribute(name) {
  var observers = [];
  function register(observer) {
    if (observer[name] == undefined) {
        var e = new Error("Not an observer: " + observer.toString() + "." + name);
        e.message += "\n" + e.stack;
        throw e;
    }
    observers.push(observer);
  }
  register.observers = observers;
  register.name = name;
  register.notify = function (a, b, c, d, e, f, g) {
    observers.copy().forEach(function(observer) {
      observer[name](a, b, c, d, e, f, g);
    });
  }
  register.unregister = function(observer) {
    observers.remove(observer);
  }
  return register;
}
