function FallingItemView(root) {
  this.root = root;
  this.element = createChildOf(root, "item");
}

FallingItemView.prototype.changedPosition = function(item) {
  this.element.style.left = item.getLeft() + "%";
  this.element.style.top = item.getTop() + "%";
//  this.element.style.width =  item.getRadius() * 2 + "%";
//  this.element.style.height =  item.getRadius() * 2 + "%";
  this.element.innerText = item.getNumber() + "";
  this.element.id = "item-" + item.getNumber() + "";
}

FallingItemView.prototype.itemDestroyed = function() {
  this.root.removeChild(this.element);
}

FallingItemView.prototype.tagChanged = function(tags) {
  tags.adjustClassList(this.element.classList);
}

FallingItemView.prototype.observe = function(item) {
  item.onChangedPosition(this);
  item.onItemDestroyed(this);
  item.tags.onTagChanged(this);
  this.changedPosition(item);
  this.tagChanged(item.tags);
}
