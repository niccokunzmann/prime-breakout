
function FallingItem(collectionStrategy, position, settings) {
  this.collectionStrategy = collectionStrategy;
  this.onItemDestroyed = ObserverAttribute("itemDestroyed");
  this.onItemTouchesGround = ObserverAttribute("itemTouchesGround");
  this.onItemCollected = ObserverAttribute("itemCollected");
  this.onItemMissed = ObserverAttribute("itemMissed");
  this.onChangedPosition = ObserverAttribute("changedPosition");
  this.settings = settings;
  this.tags = new TagSet();
  this.x = position.x;
  this.y = position.y;
  this.wasCollected = false;
}

FallingItem.prototype.forPoints = function() {
  if (this.tags.positivePoints.isActive()) {
    return this.collectionStrategy.positive();
  }
  return this.collectionStrategy;
}

FallingItem.prototype.forAchievements = function() {
  return this.collectionStrategy.getNumber();
}

FallingItem.prototype.getSpeed = function() {
  return this.settings.velocity;
}

FallingItem.prototype.step = function() {
  this.y += this.getSpeed();
  this.onChangedPosition.notify(this);
  if (this.getBottom() > this.settings.maxY) {
    this.onItemTouchesGround.notify(this);
  }
  if (this.getTop() > this.settings.maxY) {
    this.destroy();
  }
}

FallingItem.prototype.getRadius = function() {
  return this.settings.radius;
}

FallingItem.prototype.getNumber = function() {
  return this.collectionStrategy.getNumber();
}

FallingItem.prototype.getX = function() {
  return this.x;
}

FallingItem.prototype.getY = function() {
  return this.y;
}

FallingItem.prototype.getLeft = function () {
  return this.getX() - this.getRadius();
}

FallingItem.prototype.getRight = function () {
  return this.getX() + this.getRadius();
}

FallingItem.prototype.getTop = function () {
  return this.getY() - this.getRadius();
}

FallingItem.prototype.getBottom = function () {
  return this.getY() + this.getRadius();
}

FallingItem.prototype.destroy = function() {
  if (!this.wasCollected) {
    this.onItemMissed.notify(this);
  }
  this.onItemDestroyed.notify(this);
}

FallingItem.prototype.createViewOn = function(root) {
  var view = new FallingItemView(root);
  view.observe(this);
}

FallingItem.prototype.collect = function() {
  this.onItemCollected.notify(this);
  this.wasCollected = true;
  this.destroy();
}
