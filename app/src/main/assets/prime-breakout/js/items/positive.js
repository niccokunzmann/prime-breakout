function PositiveNumberStrategy(number) {
  this.number = number;
}

PositiveNumberStrategy.prototype.getNumber = function() {
  return this.number;
}

PositiveNumberStrategy.prototype.positive = function() {
  return this;
}

PositiveNumberStrategy.prototype.collected = function(points) {
  points.add(this.number)
}

PositiveNumberStrategy.prototype.missed = function(points) {
}
