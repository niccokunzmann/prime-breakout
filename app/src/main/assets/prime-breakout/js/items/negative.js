function NegativeNumberStrategy(number) {
  this.number = number;
}

NegativeNumberStrategy.prototype.getNumber = function() {
  return this.number;
}

NegativeNumberStrategy.prototype.positive = function() {
  return new PositiveNumberStrategy(this.getNumber());
}

NegativeNumberStrategy.prototype.collected = function(points) {
  points.remove(this.number)
}

NegativeNumberStrategy.prototype.missed = function(points) {
  points.rewardMiss();
}
