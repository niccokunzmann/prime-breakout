
function getChecksAndStrategies(){
  return [
    {
      use: isOne,
      strategy: NoPointsStrategy,
    },
    {
      use: is42,
      strategy: NoPointsStrategy,
    },
    {
      use: isPrime,
      strategy: PositiveNumberStrategy,
    },
    {
      use: isSquare,
      strategy: NoPointsStrategy,
    },
  ];
}

function getItemStrategyForNumber(number) {
  return getChecksAndStrategies().reduce(function(given, spec) {
    return spec.use(number) ? new spec.strategy(number) : given;
  }, new NegativeNumberStrategy(number));
}
