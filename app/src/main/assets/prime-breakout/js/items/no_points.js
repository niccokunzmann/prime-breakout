function NoPointsStrategy(number) {
  this.number = number;
}

NoPointsStrategy.prototype.getNumber = function() {
  return this.number;
}

NoPointsStrategy.prototype.positive = function() {
  return this;
}

NoPointsStrategy.prototype.collected = function(points) {
}

NoPointsStrategy.prototype.missed = function(points) {
}
