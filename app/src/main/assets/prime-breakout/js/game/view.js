
function GameView(root) {
  this.root = root;
}

GameView.prototype.observe = function (game) {
  game.onNewBall(this);
  game.onNewBlock(this);
}

GameView.prototype.newBall = function (ball) {
  ball.createViewOn(this.root);
}

GameView.prototype.newBlock = function (block) {
  block.createViewOn(this.root);
}

