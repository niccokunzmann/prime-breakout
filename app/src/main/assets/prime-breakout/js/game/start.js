var game = null;


function startGame() {
  var gameField = document.getElementById("game");
  gameField.innerHTML = "";
  var controls = new Controls();
  var modes = getCurrentGameModes();
  game = new Game(SETTINGS, modes);
  game.observeControls(controls);
  game.createViewOn(gameField);
  controls.startTheGame();
}
