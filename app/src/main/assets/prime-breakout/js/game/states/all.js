
function GameStates(settings) {
  this.settings = settings;
  this.onItemCreated = ObserverAttribute("itemCreated");
  this.onItemCollected = ObserverAttribute("itemCollected");
  this.onItemMissed = ObserverAttribute("itemMissed");

  this.onBlockCreated = ObserverAttribute("blockCreated");
  this.onBlockDestroyed = ObserverAttribute("blockDestroyed");
  
  this.createStateMachines()
}

GameStates.prototype.createStateMachines = function() {
  this.primeTwin = new PrimeTwinSM();
  this.squares = new SquareSM();
}

GameStates.prototype.enableTwins = function() {
  this.primeTwin.registerWith(this);
}

GameStates.prototype.enableSquares = function() {
  this.squares.registerWith(this);
}

GameStates.prototype.observeGame = function(game) {
  game.onItemCreated(this);
  game.onNewBlock(this);
  game.modes.enableModes(this);
}

GameStates.prototype.newBlock = function(block) {
  block.onBlockDestroyed(this);
  this.onBlockCreated.notify(block, this);
}

GameStates.prototype.blockDestroyed = function(block) {
  this.onBlockDestroyed.notify(block, this);
}

GameStates.prototype.itemCreated = function(item) {
  item.onItemMissed(this);
  item.onItemCollected(this);
  this.onItemCreated.notify(item, this);
}


GameStates.prototype.itemMissed = function(item) {
  this.onItemMissed.notify(item, this);
}

GameStates.prototype.itemCollected = function(item) {
  this.onItemCollected.notify(item, this);
}
