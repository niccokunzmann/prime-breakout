
function PrimeTwinSM() {
  this.tags = new NumberTagger();
}

PrimeTwinSM.prototype.registerWith = function (states) {
  states.onItemCollected(this);
  states.onItemCreated(this);
  states.onBlockCreated(this);
}

PrimeTwinSM.prototype.itemCollected = function (item, states) {
  var number = item.getNumber();
  if (isPrimeTwin(number) && !this.tags.isTagged(number)) {
    this.tags.tag(number + 2);
    this.tags.tag(number - 2);
  }
}

PrimeTwinSM.prototype.itemCreated = function (item) {
  this.tags.add(item.tags.primeTwin, item.getNumber());
}

PrimeTwinSM.prototype.blockCreated = function (block) {
  this.tags.add(block.tags.primeTwin, block.getNumber());
}
