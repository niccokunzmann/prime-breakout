
function NumberTagger() {
  this.taggedNumbers = [];
  this.tags = [];
}

NumberTagger.prototype.add = function (tag, number) {
  this.tags.push({
    tag: tag,
    number: number,
  });
  if (this.isTagged(number)) {
    tag.activate();
  }
}

NumberTagger.prototype.tag = function (number) {
  this.taggedNumbers.push(number);
  this.tags.forEach(function (tag) {
    if (number == tag.number) {
      tag.tag.activate();
    }
  });
}

NumberTagger.prototype.isTagged = function (number) {
  return this.taggedNumbers.includes(number);
}

