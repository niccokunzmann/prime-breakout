
function Tag(tagSet, cls) {
  this.active = false;
  this.tagSet = tagSet;
  this.tagSet.add(this);
  this.cls = cls;
}

Tag.prototype.activate = function() {
  if (!this.active) {
    this.active = true;
    this.tagSet.onTagChanged.notify(this.tagSet);
  }
}

Tag.prototype.adjustClassList = function (classList) {
  if (this.active) {
    classList.add(this.cls);
  } else {
    classList.remove(this.cls);
  }
}

Tag.prototype.isActive = function () {
  return this.active;
}

function TagSet() {
  this.tags = [];
  this.onTagChanged = ObserverAttribute("tagChanged");
  // use the same as the attribute to find it easier
  this.primeTwin = new Tag(this, "primeTwin");
  this.positivePoints = new Tag(this, "positivePoints");
  this.squareEffectStop = new Tag(this, "squareEffectStop");
}

TagSet.prototype.adjustClassList = function (classList) {
  this.tags.forEach(function(tag){
    tag.adjustClassList(classList);
  });
}

TagSet.prototype.add = function (tag) {
  this.tags.push(tag);
}
