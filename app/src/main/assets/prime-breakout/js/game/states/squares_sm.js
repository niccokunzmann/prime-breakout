
function SquareSM() {
  this.positive = new NumberTagger();
  this.marked = new NumberTagger();
}

SquareSM.prototype.registerWith = function (states) {
  states.onItemCollected(this);
  states.onItemCreated(this);
  states.onBlockCreated(this);
}

SquareSM.prototype.itemCollected = function (item, states) {
  var number = item.getNumber();
  if (isSquare(number)) {
    var root = Math.floor(Math.sqrt(number));
    var toTag = number;
    for (var i = 0; i < root; i++) {
      toTag += root;
      this.positive.tag(toTag);
      if (i % 2 == 0) {
        this.marked.tag(toTag); // tag every second one
      }
    }
    this.marked.tag(toTag); // tag the ast one
  }
}

SquareSM.prototype.itemCreated = function (item) {
  this.positive.add(item.tags.positivePoints, item.getNumber());
  this.marked.add(item.tags.squareEffectStop, item.getNumber());
}

SquareSM.prototype.blockCreated = function (block) {
  this.positive.add(block.tags.positivePoints, block.getNumber());
  this.marked.add(block.tags.squareEffectStop, block.getNumber());
}
