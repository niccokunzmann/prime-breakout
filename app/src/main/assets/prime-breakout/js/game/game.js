
function Game(settings, modes) {
  this.settings = settings;
  this.modes = modes;
  this.onStart = ObserverAttribute("start");
  this.onGameOver = ObserverAttribute("gameOver");
  this.onLevelCompleted = ObserverAttribute("levelCompleted");
  this.onNewBall = ObserverAttribute("newBall");
  this.onNewBlock = ObserverAttribute("newBlock");
  this.onItemCreated = ObserverAttribute("itemCreated");
  this.stepper = new Stepper(settings.stepper);
  this.stepper.onStep(this);
  this.onStart(this.stepper);
  this.achievements = new Achievements(settings.achievements);
  this.bar = new Bar(settings.bar);
  this.onStart(this.bar);
  this.points = new Points(settings.points);
  this.onItemCreated(this.points);
  this.achievements.onAchievementCompleted(this.points);
  this.achievements.observeGame(this);
  this.stepper.onStep(this.points);
  this.balls = [];
  this.blocks = [];
  this.items = [];
  this.blockFactory = new BlockFactory(settings.blocks);
  this.states = new GameStates();
  this.states.observeGame(this);
}

Game.prototype.createNewLevel = function () {
  var me = this;
  this.balls.copy().forEach(function (ball) {
    me.removeBall(ball);
  });
  this.addBall(new Ball(this.settings.ball));
  this.blockFactory.addBlocksTo(this);
}

Game.prototype.checkLevelComplete = function() {
  if (this.blocks.length == 0 && this.items.length == 0) {
    this.onLevelCompleted.notify(this);
  }
}

Game.prototype.blockDestroyed = function (block) {
  this.blocks.remove(block);
  this.checkLevelComplete();
}

Game.prototype.itemCreated = function (item) {
  this.items.push(item);
  item.onItemTouchesGround(this.bar);
  item.onItemDestroyed(this);
  this.onItemCreated.notify(item);
}

Game.prototype.itemDestroyed = function (item) {
  this.items.remove(item);
  this.checkLevelComplete();
}

Game.prototype.addBlock = function (block) {
  this.blocks.push(block);
  block.onBlockDestroyed(this);
  block.onItemCreated(this);
  block.registerOnStepper(this.stepper);
  this.onNewBlock.notify(block);
}

Game.prototype.addBall = function (ball) {
  this.balls.push(ball);
  ball.onBallTouchesGround(this.bar);
  ball.onBallLeftGame(this);
  this.onNewBall.notify(ball);
}

Game.prototype.removeBall = function (ball) {
  this.balls.remove(ball);
  ball.remove();
}

Game.prototype.ballLeftGame = function (ball) {
  this.removeBall(ball);
  if (this.balls.length == 0) {
    this.gameOver();
  }
}

Game.prototype.gameOver = function () {
  this.stepper.stop();
  this.onGameOver.notify(this);
}

Game.prototype.step = function () {
  //console.log("Game.step");
  if (this.controls.goRight()) {
    this.bar.goRight();
  }
  if (this.controls.goLeft()) {
    this.bar.goLeft();
  }
  var me = this;
  this.balls.copy().forEach(function(ball){
    ball.step();
    me.blocks.copy().forEach(function(block) {
      block.bounceOffBall(ball);
    });
  });
  this.items.copy().forEach(function(item) {
    item.step();
  });
}

Game.prototype.start = function () {
  this.createNewLevel();
  this.onStart.notify();
}

Game.prototype.createViewOn = function (element) {
  var view = new GameView(element);
  game.achievements.createViewOn(element);
  game.points.createViewOn(element);
  game.bar.createViewOn(element);
  game.balls.concat(game.blocks).forEach(function(model) {
    model.createViewOn(element);
  });
  view.observe(this);
}

Game.prototype.observeControls = function (controls) {
  this.controls = controls;
  this.controls.onStart(this);
  this.onGameOver(controls);
}

