
var MODE_TWINS = "twins";
var MODE_SQUARES = "squares";

function getCurrentGameModes() {
  return new GameModes(AchievementStorage.forAllGames());
}

function GameModes(storage) {
  this.storage = storage;
}

GameModes.prototype.isInMode = function(id) {
  return  !this.storage.isPersistent() || this.storage.isInMode(id);
}

GameModes.prototype.enableModes = function (achievements) {
  if (this.isInMode(MODE_TWINS)) {
    achievements.enableTwins();
  }
  if (this.isInMode(MODE_SQUARES)) {
    achievements.enableSquares();
  }
}
