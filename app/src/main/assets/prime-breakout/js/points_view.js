
function PointsView(root) {
  var container = createChildOf(root, "points");
  this.element = createChildOf(container, "text");
}

PointsView.prototype.observe = function (points) {
  this.lastPoints = points.getPoints();
  points.onPointsChanged(this);
  this.pointsChanged(points);
}

PointsView.prototype.pointsChanged = function (points) {
  if (this.lastPoints > points.getPoints() || points.getPoints() < 0) {
    this.element.classList.add("negative");
    this.element.classList.remove("positive");
  } else if (this.lastPoints < points.getPoints()) {
    this.element.classList.add("positive");
    this.element.classList.remove("negative");
  }
  this.element.innerText = points.getPoints() + "";
  this.lastPoints = points.getPoints();
}
