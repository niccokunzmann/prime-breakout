
function Ball(settings) {
  this.settings = settings;
  this.x = settings.x;
  this.y = settings.y;
  this.radius = this.settings.radius;
  this.vx = 0;
  this.vy = settings.velocity;
  this.onBallTouchesGround = ObserverAttribute("ballTouchesGround");
  this.onBallLeftGame = ObserverAttribute("ballLeftGame");
  this.onChangedPosition = ObserverAttribute("changedPosition");
  this.onBallRemoved = ObserverAttribute("ballRemoved");
}

Ball.prototype.remove = function() {
  this.onBallRemoved.notify(this);
}

Ball.prototype.step = function() {
  this.x += this.vx;
  this.y += this.vy;
  if (this.getTop() < this.settings.minY) {
    this.bounceDownOff(this.settings.minY);
  }
  if (this.getLeft() < this.settings.minX) {
    this.bounceRightOff(this.settings.minX);
  }
  if (this.getRight() > this.settings.maxX) {
    this.bounceLeftOff(this.settings.maxX);
  }
  this.onChangedPosition.notify(this);
  if (this.getBottom() > this.settings.maxY) {
    this.onBallTouchesGround.notify(this);
  }
  if (this.getTop() > this.settings.maxY) {
    this.onBallLeftGame.notify(this);
  }
}

Ball.prototype.bounceLeftOff = function(x) {
    //this.x = (x - this.radius) * 2 - this.x;
    this.vx = -Math.abs(this.vx);
}

Ball.prototype.bounceRightOff = function(x) {
    //this.x = (x + this.radius) * 2 - this.x;
    this.vx = Math.abs(this.vx);
}

Ball.prototype.bounceUpOff = function(y) {
    //this.y = (y + this.radius) * 2 - this.y;
    this.vy = -Math.abs(this.vy);
}

Ball.prototype.bounceDownOff = function(y) {
    //this.y = (y + this.radius) * 2 - this.y;
    this.vy = Math.abs(this.vy);
}

Ball.prototype.isCollidingWithVerticalLine = function(x) {
  return this.x + this.radius >= x && this.x - this.radius <= x;
}

Ball.prototype.isCollidingWithHorizontalLine = function(y) {
  return this.y + this.radius >= y && this.y - this.radius <= y;
}

Ball.prototype.getVelocity = function () {
  return Math.sqrt(this.vx * this.vx + this.vy * this.vy);
}

Ball.prototype.bounceOffPoint = function (point) {
  var velocity = this.getVelocity();
  var dx = this.x - point.x;
  var dy = this.y - point.y;
  var scale = velocity / Math.sqrt(dx*dx + dy*dy);
  this.vx = dx * scale;
  this.vy = dy * scale;
}

Ball.prototype.getRadius = function () {
  return this.x;
}

Ball.prototype.getX = function () {
  return this.x;
}

Ball.prototype.getY = function () {
  return this.y;
}

Ball.prototype.getLeft = function () {
  return this.x - this.radius;
}

Ball.prototype.getRight = function () {
  return this.x + this.radius;
}

Ball.prototype.getTop = function () {
  return this.y - this.radius;
}

Ball.prototype.getBottom = function () {
  return this.y + this.radius;
}

Ball.prototype.isCollidingWithPoint = function (point) {
  var dx = this.x - point.x;
  var dy = this.y - point.y;
  return dx * dx + dy * dy <= this.radius;
}

Ball.prototype.createViewOn = function (element) {
  var view = new BallView(element);
  view.observe(this);
}

