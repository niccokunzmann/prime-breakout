
var SCOREBOARD_VERSION = 0;

function Scoreboard(storage) {
  this.storage = storage;
  this.onScoreboardUpdated = ObserverAttribute("scoreboardUpdated");
  this.load();
}

Scoreboard.local = function () {
  return new Scoreboard(LOCAL_SCORES);
}

Scoreboard.global = function () {
  return new Scoreboard(GLOBAL_SCORES);
}

Scoreboard.prototype.scoreForLastGame = function (name) {
  var last = AchievementStorage.forLastGame();
  var all = AchievementStorage.forAllGames();
  return Score.fromAchievements(all.getGamesPlayed(), last, name);
}

Scoreboard.prototype.updateFromHighscore = function(highscore) {
  var me = this;
  console.log("update from", highscore);
  this.load();
  // remove outdated scores
  this.forEach(function(score) {
    if (score.isOutdatedBy(highscore.source, highscore.version)) {
      me.remove(score, true);
    }
  });
  var toReplace = this.scores.filter(function(score){
    return !score.isGlobal();
  });
  // add new scores
  highscore.scores.forEach(function(scoreData) {
    var score = Score.fromHighscore(highscore.source, highscore.version, scoreData);
    if (toReplace.some(function(localScore){
      return localScore.equals(score);
    })) {
      me.remove(score);
    }
    me.add(score, true);
  });
  this.store();
  this.onScoreboardUpdated.notify(this);
}

Scoreboard.prototype.includes = function(score, noUpdate) {
  if (!noUpdate) {
    this.load();
  }
  return this.scores.some(function(stored) {
    return stored.equals(score);
  });
}

Scoreboard.prototype.add = function (score, noUpdate) {
  if (!noUpdate) {
    this.load();
  }
  if (!this.includes(score, noUpdate)) {
    this.scores.push(score);
  }
  if (!noUpdate) {
    this.store();
    this.onScoreboardUpdated.notify(this);
  }
}

Scoreboard.prototype.remove = function (score, noUpdate) {
  if (!noUpdate) {
    this.load();
  }
  var me = this;
  this.scores.copy().forEach(function (otherScore) {
    if (score.equals(otherScore)) {
      me.scores.remove(otherScore);
    }
  });
  if (!noUpdate) {
    this.store();
    this.onScoreboardUpdated.notify(this);
  }
}

Scoreboard.prototype.load = function () {
  var value = this.storage.get();
  if (!value || value.version != SCOREBOARD_VERSION) {
    this.scores = [];
  } else {
    this.scores = value.scores.map(function(json) {
      return Score.fromJSON(json);
    })
  }
}

Scoreboard.prototype.store = function () {
  this.storage.set({
    version: SCOREBOARD_VERSION,
    scores: this.scores.map(function(score){
      return score.toJSON();
    }),
  })
}

Scoreboard.prototype.forEach = function (callback) {
  this.load();
  var called = [];
  this.scores.sort(function(a, b) {
    return a.getPoints() < b.getPoints();
  }).forEach(function(score) {
    if (!called.some(function(calledScore) {
      return score.looksLike(calledScore);
    })) {
      callback(score, called.length);
      called.push(score);
    }
  });
}

Scoreboard.prototype.joinedWith = function (other) {
  return new JoinedScoreboard(this, other, JoinedScoreboard.JOIN);
}

Scoreboard.prototype.replacedWith = function (other) {
  return new JoinedScoreboard(this, other, JoinedScoreboard.REPLACE);
}
