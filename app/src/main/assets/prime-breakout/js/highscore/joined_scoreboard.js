
function JoinedScoreboard(board1, board2, walkingStrategy) {
  this.board1 = board1;
  this.board2 = board2;
  this.walkingStrategy = walkingStrategy;
}

JoinedScoreboard.prototype.forEach = function(callback) {
  this.walkingStrategy(this, callback);
}

JoinedScoreboard.JOIN = function(joinedBoard, callback) {
  joinedBoard.walk(callback, true);
}

JoinedScoreboard.REPLACE = function(joinedBoard, callback) {
  joinedBoard.walk(callback, false);
}

JoinedScoreboard.prototype.walk = function (callback, add) {
  var scores = [];
  this.board1.forEach(function(score) {
    scores.push(score);
  });
  this.board2.forEach(function(score) {
    var i = 0;
    // find highscore with values
    while (i < scores.length && scores[i].getPoints() > score.getPoints()) {
      i++;
    }
    // skip equal values
    while (i < scores.length && scores[i].getPoints() == score.getPoints()) {
      if (scores[i].looksLike(score)) {
        // replace if score is present
        scores[i] = score;
        return;
      }
      i++;
    }
    if (add) {
      // insert at end of equal values
      scores.splice(i, 0, score);
    }
  });
  scores.forEach(callback);
}
