
var HIGHSCORE_API_VERSION = 1;
var DEFAULT_HIGHSCORE_SERVER_URL = "https://prime-breakout-highscore.herokuapp.com/update_highscore.js";

function HighscoreSource(name, url, canUpdate, isDefault) {
  this.name = name;
  this.url = url;
  this.canUpdate = canUpdate;
  this.isDefault = isDefault;
  this.onSourceUpdated = ObserverAttribute("sourceUpdated");
}

HighscoreSource.forUrl = function (url) {
  return new HighscoreSource(url, url, true, false);
}

HighscoreSource.local = function () {
  return new HighscoreSource("local", "js/highscore/update_highscore.js", false, true);
}

HighscoreSource.onGitlab = function () {
  return new HighscoreSource("GitLab", "https://niccokunzmann.gitlab.io/prime-breakout-highscore-server/update_highscore.js", false, document.location.hostname.toLowerCase().endsWith(".gitlab.io"));
}

HighscoreSource.onGithub = function () {
  return new HighscoreSource("GitHub", "https://niccokunzmann.github.io/prime-breakout-highscore-server/update_highscore.js", false, document.location.hostname.toLowerCase().endsWith(".github.io"));
}

HighscoreSource.onHeroku = function () {
  return new HighscoreSource("Heroku Highscore Server", DEFAULT_HIGHSCORE_SERVER_URL, true);
}

HighscoreSource.onDefaultServer = function () {
  return HighscoreSource.onHeroku();
}

HighscoreSource.all = function () {
  return [
    HighscoreSource.local(),
    HighscoreSource.onGitlab(),
    HighscoreSource.onGithub(),
    HighscoreSource.onHeroku(),
  ];
}

HighscoreSource.prototype.update = function (scoreboard) {
  this.updateFromUrl(scoreboard, this.url);
}

HighscoreSource.prototype.updateFromUrl = function (scoreboard, url) {
  console.log("updating from " + url);
  var script = document.createElement("script");
  script.setAttribute("type", "text/javascript");
  script.setAttribute("src", url);
  var me = this;
  script.onload = function() {
    if (lastHighScore == null) {
      console.log("failed to load", url);
      return;
    }
    console.log("loaded", url);
    scoreboard.updateFromHighscore(lastHighScore);
    me.onSourceUpdated.notify(me, lastHighScore);
    lastHighScore = null;
  }
  document.head.appendChild(script);
}

var lastHighScore = null;
function update_highscore(highscore) {
  if (highscore.apiVersion == HIGHSCORE_API_VERSION) {
    lastHighScore = highscore;
  }
}

HighscoreSource.prototype.addScore = function (score, scoreboard) {
  var scores = [score.forHighscoreServer()];
  var data = encodeURIComponent(JSON.stringify(scores));
  var url = this.url + "?scores=" + data;
  this.updateFromUrl(scoreboard, url);
}

HighscoreSource.prototype.getDisplayName = function () {
  return this.name;
}

HighscoreSource.prototype.getUrl = function () {
  return this.url;
}

HighscoreSource.prototype.isDefaultServer = function () {
  return this.url == DEFAULT_HIGHSCORE_SERVER_URL;
}

HighscoreSource.prototype.equals = function (other) {
  return this.url == other.url;
}