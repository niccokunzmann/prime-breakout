
var NAME_MAX_LENGTH = 40;
var NAME_MIN_LENGTH = 2;
var NAME_FILL = "😼";
var ID_SHOW_GLOBAL_HIGHSCORE = "show-global";

function toggleGlobalHighscore() {
  var link = document.getElementById(ID_SHOW_GLOBAL_HIGHSCORE);
  link.classList.toggle("active");
  fillInHighScore();
}

function useGLobalHighScore() {
  var link = document.getElementById(ID_SHOW_GLOBAL_HIGHSCORE);
  return link.classList.contains("active");
}

function hideNewEntry() {
  var elements = document.getElementsByClassName("new-entry");
  for (var i = 0; i < elements.length; i++) {
    var element = elements[i];
    element.parentElement.removeChild(element);
  }
}

function fillInSubmitValues() {
  var board = subscribeTo(Scoreboard.local());
  var newEntry = board.scoreForLastGame(PLAYER_NAME.get());
  if (board.includes(newEntry) || newEntry.getPoints() <= 0) {
    console.log("entry exists", newEntry);
    hideNewEntry();
    return;
  }
  var points = document.getElementById("points");
  points.innerText = newEntry.getPoints() + "";
  var textInput = document.getElementById("name");
  textInput.value = PLAYER_NAME.get();
  var button = document.getElementById("add");
  function submitEntry() {
    var name = textInput.value;
    if (name.length > NAME_MAX_LENGTH) {
      textInput.value = name.slice(0, NAME_MAX_LENGTH);
      return;
    }
    if (name.length < NAME_MIN_LENGTH) {
      textInput.value += NAME_FILL;
      return;
    }
    PLAYER_NAME.set(name);
    var score = board.scoreForLastGame(PLAYER_NAME.get());;
    board.add(score);
    hideNewEntry();
  }
  
  button.onclick = submitEntry;
  textInput.onsubmit = submitEntry;
}

function fillInHighScore() {
  var board = subscribeTo(Scoreboard.local());
  var globalBoard = subscribeTo(Scoreboard.global());
  var current = board.scoreForLastGame(PLAYER_NAME.get());
  var table = document.getElementById("score-board");
  table.innerHTML = "";
  var i = 0;
  if (useGLobalHighScore()){
    board = board.joinedWith(globalBoard);
  } else {
    board = board.replacedWith(globalBoard);
  }
  board.forEach(function(score) {
    i++;
    var element = score.createTableRow();
    table.appendChild(element);
    if (current.looksLike(score)) {
      element.classList.add("last-game");
    }
    if (i % 2 == 1) {
      element.classList.add("odd");
    }
    score.onScoreBecomesGlobal({
      scoreBecomesGlobal: scoreBecomesGlobal
    });
    if (score.getPoints() == current.getPoints()) {
      //element.classList.add("equal-game");
      element.scrollIntoView();
    }
  });
}

function scoreBecomesGlobal(score) {
  getHighscoreServer().addScore(score, subscribeTo(Scoreboard.global()));
}

function subscribeTo(scoreboard) {
  scoreboard.onScoreboardUpdated({
    "scoreboardUpdated": fillInHighScore
  });
  return scoreboard;
}

/* load the local source and maybe gitlab if we are served from there. */
function loadDefaultHighscoreSources() {
  var board = subscribeTo(Scoreboard.global());
  HighscoreSource.all().forEach(function(source) {
    if (source.isDefault) {
      source.update(board);
    }
  });
}

function toggleSettings() {
  var settings = document.getElementById("settings");
  settings.classList.toggle("hidden");
}

var updateSourceFromDefaultServer = null;

function highsoreSourceToListItem(source) {
  var li = document.createElement("li");
  var a = document.createElement("a");
  a.innerText = source.getDisplayName();
  var used = document.createElement("span");
  used.classList.add("used");
  used.innerText = source.isDefault ? "☑" : "☐";
  li.appendChild(used);
  li.appendChild(a);
  function updateSource() {
    var board = subscribeTo(Scoreboard.global());
    source.update(board);
    used.innerText = "↻";
  }
  a.onclick = updateSource;
  if (source.isDefaultServer()) {
    updateSourceFromDefaultServer = updateSource;
  }
  source.onSourceUpdated({
    sourceUpdated: function(highscore, data) {
      if (data.error) {
        console.log(data);
        used.innerText = "✗";
      } else {
        used.innerText = "☑";
      }
    }
  });
  return li;
}

function fillInSettings() {
  var list = document.getElementById("highscore-sources-list");
  HighscoreSource.all().forEach(function (source) {
    var listItem = highsoreSourceToListItem(source);
    list.appendChild(listItem);
  });
  var urlInput = document.getElementById("highscore-server");
  urlInput.placeholder = HighscoreSource.onDefaultServer().getUrl();
  urlInput.value = HIGHSCORE_SERVER_URL.get();
  urlInput.onchange = function() {
    HIGHSCORE_SERVER_URL.set(urlInput.value);
  }
}

function getHighscoreServer() {
  var urlInput = document.getElementById("highscore-server");
  if (urlInput.value == "") {
    return HighscoreSource.onDefaultServer();
  }
  return HighscoreSource.forUrl(urlInput.value);
}

function updateHighscoreFromDefaultServer() {
  var board = subscribeTo(Scoreboard.global());
  var source = getHighscoreServer();
  if (source.isDefaultServer()) {
    // also update the listing of sources
    updateSourceFromDefaultServer();
    return;
  }
  source.update(board);
}

window.addEventListener("load", function() {
  fillInSubmitValues();
  fillInHighScore();
  loadDefaultHighscoreSources();
  fillInSettings();
});
