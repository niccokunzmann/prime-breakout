/* generated */
update_highscore({
  "apiVersion": 1,
  "source": "prime-breakout-highscore.herokuapp.com/update_highscore.js",
  "version": 6,
  "scores": [
    {
      "id": 1,
      "name": "Example Beginner",
      "points": 0
    },
    {
      "id": 2,
      "name": "Example Player",
      "points": 42
    },
    {
      "id": 3,
      "name": "Example Player",
      "points": 100
    },
    {
      "id": 3,
      "name": "Test",
      "points": 111
    },
    {
      "id": 3,
      "name": "Test",
      "points": 222
    },
    {
      "id": 3,
      "name": "Test",
      "points": 11
    },
    {
      "id": 3,
      "name": "Test",
      "points": 1
    }
  ]
});