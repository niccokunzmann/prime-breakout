
function Score(id, name, points, global) {
  this.id = id;
  this.name = name;
  this.points = points;
  this.global = global || false;
  this.onScoreBecomesGlobal = ObserverAttribute("scoreBecomesGlobal");
}

Score.fromAchievements = function (id, achievementStorage, name) {
  return new Score(id, name, achievementStorage.getMaximumPoints(), false);
}

Score.fromHighscore = function (source, version, data) {
  return new Score(data.id, data.name, data.points, {
    source: source,
    version: version
  });
}

Score.fromJSON = function(json) {
  return new Score(json.id, json.name, json.points, json.global);
}

Score.prototype.isOutdatedBy = function(source, version) {
  return this.isGlobal() && this.global.source == source && this.global.version < version;
}

Score.prototype.toJSON = function () {
  return {
    name: this.name,
    points: this.points,
    id: this.id,
    global: this.global,
  }
}

Score.prototype.equals = function (other) {
  if (this.isGlobal()) {
    if (other.isGlobal() && other.global.source != this.global.source) {
      return false;
    }
    return this.looksLike(other);
  }
  if (other.isGlobal()) {
    return other.equals(this);
  }
  return this.id == other.id;
}

Score.prototype.looksLike = function (other) {
  return this.name == other.name && this.points == other.points;
}

Score.prototype.getPoints = function () {
  return this.points;
}

Score.prototype.isGlobal = function () {
  return this.global ? true : false;
}

Score.prototype.createTableRow = function() {
  var tr = document.createElement("tr");
  var name = document.createElement("td");
  tr.appendChild(name);
  var points = document.createElement("td");
  tr.appendChild(points);
  var global = document.createElement("td");
  tr.appendChild(global);
  
  name.innerText = this.name;
  points.innerText = this.points + "";
  if (this.isGlobal()) {
    global.innerText = "✓";
    tr.classList.add("global-score");
  } else {
    var submit = document.createElement("a");
    submit.classList.add("button")
    var me = this;
    submit.onclick = function() {
      me.onScoreBecomesGlobal.notify(me);
      submit.innerText = "...";
      submit.classList.add("uploading");
    }
    submit.innerText = "✗ submit";
    global.appendChild(submit);
  }
  return tr;
}

Score.prototype.forHighscoreServer = function () {
  return {
    name: this.name,
    id: this.id,
    points: this.points
  }
}
