/* shortcuts and common functions */

function createChildOf(root, cls) {
  var element = document.createElement("div");
  element.classList.add(cls);
  element.classList.add("noselect");
  root.appendChild(element);
  return element;
}

// used until replaced
function translate(key, defaultValue) {
  return defaultValue == undefined ? key : defaultValue;
}
