
function Stepper(settings) {
  this.milliseconds = settings.millisecondsBetweenSteps;
  this.id = null;
  this.onStep = ObserverAttribute("step");
}

Stepper.prototype.start = function() {
  var me = this;
  if (this.isRunning()) {
    throw new Error("Cannot start stepper, it is already running.");
  }
  this.id = setInterval(function() {
    me.onStep.notify();
  }, this.milliseconds);
 }

Stepper.prototype.stop = function() {
  if (this.isRunning()) {
    clearInterval(this.id);
    this.id = null;
  }
}

Stepper.prototype.isRunning = function() {
  return this.id != null;
}
