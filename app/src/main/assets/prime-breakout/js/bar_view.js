function BarView(root) {
  this.element = createChildOf(root, "bar");
}

BarView.prototype.changePosition = function(bar) {
  this.element.style.left = bar.getX() + "%";
  this.element.style.width = bar.getWidth() + "%";
}

BarView.prototype.observe = function(bar) {
  bar.onChangePosition(this);
}
