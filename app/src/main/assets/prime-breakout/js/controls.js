function hide(element) {
  element.classList.add("hidden");
}
function show(element) {
  element.classList.remove("hidden");
}

/* Generalize input of events for rigth and left.
 */
function isLeftEvent(event) {
  return event.keyIdentifier == "Left" || event.key == "ArrowLeft";
}

function isRightEvent(event) {
  return event.keyIdentifier == "Right" || event.key == "ArrowRight";
}

function Controls() {
  var me = this;
  this.touching = false;
  this.touchLeft = false;
  this.field = document.getElementById("game");
  function touch(event) {
    me.touching = event.touches.length == 1;
    if (me.touching) {
      me.touchPosition(event.touches[0].clientX, event.touches[0].clientY);
    }
  }
  function notouch() {
    me.touching = false;
  }
  this.field.ontouchmove = touch;
  this.field.ontouchstart = touch;
  this.field.ontouchend = notouch;
  this.field.ontouchcancel = notouch;
  
  document.body.onkeydown = function(event) {
    //console.log(event);
    if (isLeftEvent(event)) {
      me.keyDownLeft = true;
    }
    if (isRightEvent(event)) {
      me.keyDownRight = true;
    }
  }
  document.body.onkeyup = function(event) {
    //console.log(event);
    if (isLeftEvent(event)) {
      me.keyDownLeft = false;
    }
    if (isRightEvent(event)) {
      me.keyDownRight = false;
    }
  }
  
  this.menu = document.getElementById("menu");
  
  this.onStart = ObserverAttribute("start");
  this.onStart(this);
  
  this.showMenu();
}

Controls.prototype.start = function() {
  show(this.field);
  hide(this.menu);
}

Controls.prototype.gameOver = function() {
  this.showMenu();
}

Controls.prototype.startTheGame = function() {
  this.onStart.notify();
}


Controls.prototype.showMenu = function() {
  hide(this.field);
  show(this.menu);
}


Controls.prototype.touchPosition = function(x, y) {
  // view port height, see https://stackoverflow.com/a/8876069/1320237
  const width = Math.max(this.field.clientHeight, this.field.innerHeight || 0);
  this.touchLeft = x < width / 2;
}

Controls.prototype.goRight = function() {
  return !this.keyDownLeft && (
      this.touching && !this.touchLeft ||
      !this.touching && this.keyDownRight);
}
Controls.prototype.goLeft = function() {
  return !this.keyDownRight && (
      this.touching && this.touchLeft ||
      !this.touching && this.keyDownLeft);
}
