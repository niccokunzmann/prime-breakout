
function GlobalStoredValue(key, defaultValue) {
  this.key = key;
  this.defaultValue = defaultValue;
  this.resetValue = defaultValue;
}

var LAST_GAME = new GlobalStoredValue("last-game", null);
var ALL_GAMES = new GlobalStoredValue("all-games", null);
var LOCAL_SCORES = new GlobalStoredValue("local-scores", null);
var GLOBAL_SCORES = new GlobalStoredValue("global-scores", null);
var PLAYER_NAME = new GlobalStoredValue("player-name", "");
var HIGHSCORE_SERVER_URL = new GlobalStoredValue("highscore-server-url", "");


function setKeyValue(key, value) {
  if (localStorage) {
    localStorage[key] = JSON.stringify(value);
  }
}
 
function getKeyValue(key, defaultValue) {
  if (localStorage) {
    var value = localStorage[key];
    if (value === undefined) {
      return defaultValue;
    }
    return JSON.parse(value);
  }
  return defaultValue;
}

function hasKeyValueStorage() {
  return localStorage != undefined;
}


GlobalStoredValue.prototype.set = function(newValue) {
  setKeyValue(this.key, newValue);
  this.defaultValue = newValue;
}

GlobalStoredValue.prototype.isPersistent = function() {
  return hasKeyValueStorage();
}

GlobalStoredValue.prototype.get = function() {
  return getKeyValue(this.key, this.defaultValue);
}

GlobalStoredValue.prototype.reset = function() {
  this.set(this.resetValue);
}
