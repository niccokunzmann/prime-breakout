function divisionChallenge(n, cls) {
  // these challenges are connected with the number_helpers.isChallenge
  return {
    number: n,
    mode: "squares",
    stages: [
      {
        cls: "tutorial",
        count: 1,
        reward: 0,
        message: "Show that you know the division rule for " + n + " and collect " + n + "⨉" + n + " to " + n + "⨉" + n*2 + "."
      },
      {
        cls: cls,
        count: n + 1,
        reward: n*n,
        id: "challenge-" + n,
        message: "You collected the " + n + " numbers bigger than " + n*n + " that can be divided by " + n + "!"
      },
    ],
  };
}

var SETTINGS = {
  achievements: {
    level: {
      message: "Level 0 complete!",
      start: 1,
    },
    gameOver: {
      messageFirstGame: "Game Over!",
      messageGamesCompleted: "Game 0 Over!"
    },
    one: {
      stages: [
        {
          cls: "error",
          count: 1,
          reward: -8,
          id: "one",
          message: "1 is not a prime number!"
        },
      ]
    },
    number42: {
      stages: [
        {
          cls: "silver",
          count: 1,
          id: "42",
          reward: 42,
          message: "Yeah, 42 is not really a prime number but it is special! You collected it. Only, this game will not answer the question of all questions."
        },
      ]
    },
    smallTimesTables: {
      stages: [
        {
          cls: "silver",
          count: 32, // 42 - 10
          id: "small-times-tables",
          reward: 100,
          message: "You avoided all numbers in the small times tables!"
        },
      ]
    },
    bigTimesTables: {
      stages: [
        {
          cls: "gold",
          count: 132, // 152 - 20
          id: "big-times-tables",
          reward: 1000,
          message: "You avoided all numbers in the big times tables!"
        },
      ]
    },
    evenNumbers: {
      stages: [
        {
          cls: "tutorial",
          count: 1,
          reward: 2,
          message: "Tutorial: one even number avoided!"
        },
        {
          cls: "bronze",
          count: 10,
          reward: 10,
          id: "even-numbers-bronze",
          message: "Avoided 10 even numbers!"
        },
        {
          cls: "silver",
          count: 20,
          reward: 20,
          id: "even-numbers-silver",
          message: "Avoided 20 even numbers!"
        },
        {
          cls: "gold",
          count: 50,
          reward: 50,
          id: "even-numbers-gold",
          message: "Avoided 50 even numbers!"
        },
      ],
    },
    multiplesOf3: {
      stages: [
        {
          cls: "bronze",
          count: 9,
          reward: 15,
          id: "3-bronze",
          message: "Avoided 9 multiples of 3!"
        },
        {
          cls: "silver",
          count: 21,
          reward: 33,
          id: "3-silver",
          message: "Avoided 21 multiples of 3!"
        },
        {
          cls: "gold",
          count: 33,
          reward: 66,
          id: "3-gold",
          message: "Avoided 33 multiples of 3!"
        },
      ],
    },
    multiplesOf5: {
      stages: [
        {
          cls: "bronze",
          count: 20,
          reward: 100,
          id: "5-bronze",
          message: "Avoided 20 multiples of 5!"
        },
      ]
    },
    multiplesOf11: {
      stages: [
        {
          cls: "bronze",
          count: 3,
          reward: 33,
          id: "11-bronze",
          message: "Avoided 3 multiples of 11!"
        },
        {
          cls: "silver",
          count: 9,
          reward: 99,
          id: "11-silver",
          message: "Avoided 9 multiples of 11!"
        },
        {
          cls: "gold",
          count: 20,
          reward: 220,
          id: "11-gold",
          message: "Avoided 20 multiples of 11!"
        },
      ],
    },
    squares: {
      mode: "squares",
      stages: [
        {
          cls: "tutorial",
          count: 1,
          reward: 0,
          message: "Tutorial: You caught a square number! The next √n multiples of √n yield points & every second one is marked green."
        },
        {
          cls: "bronze",
          id: "squares-bronze",
          count: 5,
          reward: 125,
          message: "You collected 5 square numbers!"
        },
        {
          cls: "silver",
          id: "squares-silver",
          count: 10,
          reward: 250,
          message: "You collected 10 square numbers!"
        },
        {
          cls: "gold",
          id: "squares-gold",
          count: 20,
          reward: 1000,
          message: "You collected 20 square numbers!"
        },
      ]
    },
    primeNumbers: {
      stages: [
        {
          cls: "tutorial",
          count: 1,
          reward: 11,
          message: "Tutorial: You caught a prime number!"
        },
        {
          cls: "bronze",
          count: 10,
          reward: 41,
          id: "prime-numbers-bronze",
          message: "Caught 10 prime numbers!"
        },
        {
          cls: "silver",
          count: 20,
          reward: 101,
          id: "prime-numbers-silver",
          message: "Caught 20 prime numbers!"
        },
        {
          cls: "gold",
          count: 50,
          reward: 500,
          id: "prime-numbers-gold",
          message: "Caught 50 prime numbers!"
        },
      ],
    },
    primeTwins: {
      mode: "twins",
      stages: [
        {
          cls: "tutorial",
          count: 1,
          reward: 0,
          message: "Tutorial: You collected a prime number twin! The possible twins are highlighted. Do not catch the wrong ones!"
        },
        {
          cls: "bronze",
          count: 7,
          reward: 41,
          id: "prime-twins-bronze",
          message: "Caught 7 prime number twins!"
        },
        {
          cls: "silver",
          count: 13,
          reward: 101,
          id: "prime-twins-silver",
          message: "Caught 13 prime number twins!"
        },
        {
          cls: "gold",
          count: 19,
          reward: 500,
          id: "prime-twins-gold",
          message: "Caught 19 prime number twins!"
        },
      ],
    },
    challenges: [
      divisionChallenge(2, "bronze"),
      divisionChallenge(3, "bronze"),
      divisionChallenge(4, "silver"),
      divisionChallenge(5, "silver"),
      divisionChallenge(6, "silver"),
      divisionChallenge(8, "gold"),
      divisionChallenge(9, "gold"),
      divisionChallenge(11, "gold"),
    ],
    points: {
      modes: [
        {
          points: 100,
          mode: "twins",
        },
        {
          points: 300,
          mode: "squares",
        },
      ],
    },
  },
  stepper: {
    millisecondsBetweenSteps: 20,
  },
  bar: {
    size: 10,
    speed: 3,
    startX: 45,
    minX: 0,
    maxX: 100,
    width: 10,
    y: 100,
    // how much below the bar should the ball bounce off
    yBounceOffset: 10,
  },
  ball: {
    radius: 2,
    x: 50,
    y: 95,
    velocity: -0.7,
    maxX: 100,
    minX: 0,
    maxY: 100,
    minY: 0,
  },
  blocks: {
    rows: 4,
    columns: 8,
    minX: 0,
    maxX: 100,
    minY: 0,
    maxY: 40,
    spaceBetweenBlocksX: 2,
    spaceBetweenBlocksY: 2,
    initialBlockNumber: 1,
    items: {
      velocity: 0.4,
      maxY: 100,
      radius: 2,
    },
    movement : {
      maxVelocity: 0.2,
      minX: -3,
      maxX: 103,
    }
  },
  points: {
    min: -10,
    start: 0,
    "reward for missed non-prime number": 1,
    splitUpPointsIntoPieces: 20,
  },
};
